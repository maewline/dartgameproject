import 'dart:io';
import 'package:chalkdart/chalk.dart';
import 'Game.dart';
import 'Player.dart';
import 'RandomWord.dart';
import 'Table.dart';
import 'main.dart';

class FindWord extends Game {
  Player? player;
  Table? table;
  Stopwatch? countTime;
  FindWord() {
    table = Table();
    countTime = Stopwatch();
  }

  @override
  void showWelcome() {
    clearScreen();
    print(chalk.red("""
███████╗██╗███╗   ██╗██████╗     ██╗    ██╗ ██████╗ ██████╗ ██████╗      ██████╗  █████╗ ███╗   ███╗███████╗
██╔════╝██║████╗  ██║██╔══██╗    ██║    ██║██╔═══██╗██╔══██╗██╔══██╗    ██╔════╝ ██╔══██╗████╗ ████║██╔════╝
█████╗  ██║██╔██╗ ██║██║  ██║    ██║ █╗ ██║██║   ██║██████╔╝██║  ██║    ██║  ███╗███████║██╔████╔██║█████╗  
██╔══╝  ██║██║╚██╗██║██║  ██║    ██║███╗██║██║   ██║██╔══██╗██║  ██║    ██║   ██║██╔══██║██║╚██╔╝██║██╔══╝  
██║     ██║██║ ╚████║██████╔╝    ╚███╔███╔╝╚██████╔╝██║  ██║██████╔╝    ╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗
╚═╝     ╚═╝╚═╝  ╚═══╝╚═════╝      ╚══╝╚══╝  ╚═════╝ ╚═╝  ╚═╝╚═════╝      ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝
                                                                                                            
"""));
    print("Hi ${player!.name} Welcome to FindWord Game!!");
  }

  void setPlayer(Player player) {
    this.player = player;
  }

  @override
  void showRule() {
    print("\nRule Of FindWord Game:");
    print("- This is a text search game. You'll need a long time to find it.");
    print(
        "- Enter the inputs in sequential order, starting with the row and then the column.");
    print(
        "- If you think you will give up You just type the word ${chalk.red.bold.blink("giveup")} and the game is over.");
    print(
        "you can win only when You have to find all the words in the list. But if you give up, you are the loser.");
    print(
        "- If you already understand the FindWord rules, please type Enter to start the game.");
    var input = stdin.readLineSync();
  }

  void process() {
    table?.prepareTable();
    table?.randomWord();
    table?.randomWordIntoTable();
    table?.fillNull();
    while (table!.result.isNotEmpty && table?.giveUp == false) {
      table?.showTable();
      table?.showReaminingWord();
      table?.showResult();
      stdout.write("Please input your answer: ");
      String input = stdin.readLineSync()!;
      table?.checkResult(input);
    }
    if (table!.remainingWord <= 0) {
      print("You win");
      player!.increaseWin();
    } else {
      print("You lose");
      player!.increaseLose();
    }
  }

  void showStatus() {
    print(player);
    print("Time: ${countTime?.elapsed.toString().substring(0, 7)}");
  }

  void resetGame() {
    table = Table();
    countTime = Stopwatch();
    
  }

  void play() {
    while (true) {
      countTime?.start();
      process();
      countTime?.stop();
      showStatus();
      if (playAgain() == true) {
        resetGame();
        continue;
      } else {
        resetGame();
        clearScreen();
        break;
      }
    }
  }

  bool playAgain() {
    stdout.write("\nDo you want to play again? (y/n): ");
    String input = stdin.readLineSync()!;
    if (input == "y") {
      return true;
    } else if (input == "n") {
      return false;
    }
    return true;
  }
}
