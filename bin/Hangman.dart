import 'dart:io';
import 'dart:math';
import 'package:chalkdart/chalk.dart';

import 'Game.dart';
import 'Player.dart';
import 'RandomWord.dart';
import 'Word.dart';

class Hangman extends Game implements RandomWord {
  String word = "";
  File? file;
  int hint = 0;
  List<String> listWords = [];
  int lettersRemaining = 0;
  List<String> enteredLetters = [];
  List<String> foundLetters = [];
  List<String> wrongLetters = [];
  int live = 0;
  Player? player;
  Word? wordParser;
  Hangman() {
    live = 6;
    player?.setHint(3);
    lettersRemaining = word.length;
  }

  void showStatus() {
    printHangMan();
    showTextInput();
    showLineWord();
  }

  void resetGame() {
    foundLetters = [];
    randomWord();
    live = 6;
    player?.setHint(3);
  }

  void setWord(String word) {
    this.word = word;
  }

  void showLineWord() {
    wordParser = Word();
    wordParser!.printWord(foundLetters, word);
  }

  void showLineWordHint() {
    wordParser = Word();
    wordParser!.printWordHint(foundLetters, word);
  }

  void showTextInput() {
    print("Please input your text");
  }

  void printHangMan() {
    if (live == 6) {
      print("  +---+");
      print("  |   |");
      print("      |");
      print("      |");
      print("      |");
      print("      |");
      print("=========");
    } else if (live == 5) {
      print("  +---+");
      print("  |   |");
      print("  O   |");
      print("      |");
      print("      |");
      print("      |");
      print("=========");
    } else if (live == 4) {
      print("  +---+");
      print("  |   |");
      print("  O   |");
      print("  |   |");
      print("      |");
      print("      |");
      print("=========");
    } else if (live == 3) {
      print("  +---+");
      print("  |   |");
      print("  O   |");
      print(" /|   |");
      print("      |");
      print("      |");
      print("=========");
    } else if (live == 2) {
      print("  +---+");
      print("  |   |");
      print("  O   |");
      print(" /|\\  |");
      print("      |");
      print("      |");
      print("=========");
    } else if (live == 1) {
      print("  +---+");
      print("  |   |");
      print("  O   |");
      print(" /|\\  |");
      print(" /    |");
      print("      |");
      print("=========");
    } else if (live == 0) {
      print("  +---+");
      print("  |   |");
      print("  O   |");
      print(" /|\\  |");
      print(" / \\  |");
      print("      |");
      print("=========");
    }
  }

  void clearScreen() {
    print("\x1B[2J\x1B[0;0H");
  }

  void prepareGame() {
    var word = Word();
    word.createFile("D:/BUU/Dart/projectgame2/bin/data/wordlist.txt");
    word.parse();
    List<String> listWord = word.getListwords();
    setListWords(listWord);
    randomWord();
  }

  void checkWord(String input) {
    if (input == "hh") {
      if (hintAble()) {
        clearScreen();
        showLineWordHint();
        print("choose position your need hint");
        int input = int.tryParse(stdin.readLineSync()!) ?? -1;
        //input error
        if (input == -1) {
          return;
        }
        String inputHint = word[input - 1];
        if (!foundLetters.contains(inputHint)) {
          checkWord(inputHint);
          player?.decreaseHint();
        }
      }
    } else if (word.contains(input) && !foundLetters.contains(input)) {
      foundLetters.add(input);
      for (var i = 0; i < word.length; i++) {
        if (word[i] == input) {
          lettersRemaining--;
        }
      }
    } else {
      live--;
    }
  }

  @override
  void showWelcome() {
    clearScreen();
    print(chalk.cyan.bold("""

██   ██  █████  ███    ██  ██████  ███    ███  █████  ███    ██      ██████   █████  ███    ███ ███████ 
██   ██ ██   ██ ████   ██ ██       ████  ████ ██   ██ ████   ██     ██       ██   ██ ████  ████ ██      
███████ ███████ ██ ██  ██ ██   ███ ██ ████ ██ ███████ ██ ██  ██     ██   ███ ███████ ██ ████ ██ █████   
██   ██ ██   ██ ██  ██ ██ ██    ██ ██  ██  ██ ██   ██ ██  ██ ██     ██    ██ ██   ██ ██  ██  ██ ██      
██   ██ ██   ██ ██   ████  ██████  ██      ██ ██   ██ ██   ████      ██████  ██   ██ ██      ██ ███████ 
                                                                                                        
                                                                                                        
                                                                                                      
"""));
    print("Hi ${player!.name} Welcome to Hangman Game!!");
  }

  @override
  void showRule() {
    print("\nRule Of Hangman Game:");
    print(
        "- This is a word guessing game. in which the player can misstake 6 times, if more than that the player loses");
    print(
        "- Players will be able to use helpers. that is a ${chalk.cyan.bold("hint")} The hint can be used 3 times. To use it, type ${chalk.cyan.bold.blink("hh")} and enter, then select the desired location.");
    print(
        "- If you already understand the hangman rules, please type Enter to start the game.");
    var input = stdin.readLineSync();
  }

  String isFinish() {
    if (live > 0 && lettersRemaining == 0) {
      //  player?.increaseWin();
      return "win";
    }
    if (live > 0) {
      return "notFinish";
    }
    if (live == 0 && lettersRemaining > 0) {
      // player?.increaseLose();
      return "lose";
    }
    return "notFinish";
  }

  void setPlayer(Player player) {
    this.player = player;
    player.hint = 3;
  }

  void setListWords(List<String> listWord) {
    listWords = listWord;
  }

  void showWordResult() {
    print("Result is: $word");
  }

  @override
  String randomWord() {
    int listSize = listWords.length;
    Random random = Random();
    int randomNumber = random.nextInt(listSize);
    word = listWords[randomNumber];
    lettersRemaining = word.length;
    return word;
  }

  void increaseHint() {
    hint++;
  }

  void showHint() {
    print("");
    print("Your Hint: ${player?.getHint()}");
  }

  bool hintAble() {
    if (player?.hint == 0) {
      return false;
    }
    print(player?.hint);
    return true;
  }

  void showInputSymbol() {
    stdout.write(">> ");
  }

  void process() {
    while (true) {
      freezeWelcomeRule();
      showHint();
      showStatus();
      showInputSymbol();
      String input = stdin.readLineSync()!;
      checkWord(input);
      print(player);
      if (isFinish() == "win") {
        freezeWelcomeRule();
        showHint();
        showStatus();
        increaseWin(player!);
        showStatusGame(player!, "win");
        break;
      } else if (isFinish() == "lose") {
        freezeWelcomeRule();
        showHint();
        showStatus();
        increaseLose(player!);
        showStatusGame(player!, "lose");
        showWordResult();
        break;
      }
    }
  }

  void showStatusGame(Player player, String result) {
    clearScreen();
    showResult(result);
    showStatus();
    print(player);
  }

  void showResult(String input) {
    freezeWelcomeRule();
    if (input == "win") {
      print("\nYou Win!!");
    } else {
      print("\nYou Lose!!");
    }
  }

  void increaseLose(Player player) {
    player.increaseLose();
  }

  void increaseWin(Player player) {
    player.increaseWin();
  }

  void freezeWelcomeRule() {
    showWelcome();
    print("\nRule Of Hangman Game:");
    print(
        "- This is a word guessing game. in which the player can misstake 6 times, if more than that the player loses");
    print(
        "- Players will be able to use helpers. that is a ${chalk.cyan.bold("hint")} The hint can be used 3 times. To use it, type ${chalk.cyan.bold.blink("hh")} and enter, then select the desired location.");
    print(
        "- If you already understand the hangman rules, please type Enter to start the game.");
  }

  void play() {
    prepareGame();
    while (true) {
      process();
      if (playAgain() == true) {
        freezeWelcomeRule();
        resetGame();
        continue;
      } else {
        resetGame();
        clearScreen();
        break;
      }
    }
  }

  bool playAgain() {
    stdout.write("\nDo you want to play again? (y/n): ");
    String input = stdin.readLineSync()!;
    if (input == "y") {
      return true;
    } else if (input == "n") {
      return false;
    }
    return true;
  }
}
