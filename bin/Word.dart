import 'dart:io';
import 'dart:math';

class Word {
  File file = File("");
  List<String> listWords = [];
  String? presentWord;
  String? word;


  List<String> getListwords() {
    return listWords;
  }

  void createFile(String path) {
    file = File(path);
  }

  void printWord(List<String> foundWord, String word) {
    String output = "";
    for (var i = 0; i < word.length; i++) {
      if (foundWord.contains(word[i])) {
        output += word[i] + " ";
      } else {
        if (word[i] == " ") {
          output += " ";
        } else {
          output += "_ ";
        }
      }
    }
    print("$output\n");
  }

  void printWordHint(List<String> foundWord, String word) {
    String on = "";
    String under = "";
    for (var i = 0; i < word.length; i++) {
      if (foundWord.contains(word[i])) {
        on += word[i] + " ";
        under += "  ";
      } else {
        on += "_ ";
        under += "${i + 1} ";
      }
    }
    print("$on");
    print("$under\n");
  }

  String? getPresentWord() {
    return "$presentWord";
  }

  void parse() {
    List<String> input = file.readAsLinesSync();
    input.forEach((element) {
      listWords.add(element);
    });
  }
}
