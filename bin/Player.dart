class Player {
  String name = "";
  int win = 0;
  int lose = 0;
  int hint = 0;
  Player(String name) {
    this.name = name;
  }
  void increaseWin() {
    win = win + 1;
  }

  void increaseLose() {
    lose = lose + 1;
  }

  int getHint() {
    return hint;
  }

  void decreaseHint() {
    hint--;
  }

  void setHint(int hint) {
    this.hint = hint;
  }

  @override
  String toString() {
    return "$name win: $win lose: $lose";
  }
}
