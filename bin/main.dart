import 'dart:io';
import 'FindWord.dart';
import 'Game.dart';
import 'Hangman.dart';
import 'Player.dart';
import 'Word.dart';

void main(List<String> args) {
  var findWord = FindWord();
  var hangman = Hangman();
  var game = Game();
  game.showWelcome();
  Player player = inputName();

  while (true) {
    game.showMainMenu();
    String select = selectGame();
    if (select == "1") {
      hangman.setPlayer(player);
      hangman.showWelcome();
      hangman.showRule();
      hangman.play();
    } else if (select == "2") {
      findWord.setPlayer(player);
      findWord.showWelcome();
      findWord.showRule();
      findWord.play();
    } else if (select == "3") {
      game.showThankYou();
      game.showGoodBye();
      break;
    } else {
      continue;
    }
  }
}

Player inputName() {
  print("What your name");
  stdout.write("Please input your name: ");
  String name = stdin.readLineSync()!;
  var player = Player(name);
  return player;
}

String selectGame() {
  print("Which game do you want to play?");
  print("1.Hangman.");
  print("2.FindWord.");
  print("3.Exit.");
  var input = stdin.readLineSync();
  if (input == "1") {
    return "1";
  } else if (input == "2") {
    return "2";
  } else if (input == "3") {
    return "3";
  }
  return "";
}

void showScore(Player player) {
  print(player);
}

void clearScreen() {
  print("\x1B[2J\x1B[0;0H");
}
