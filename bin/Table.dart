import 'dart:developer';
import 'dart:io';
import 'dart:math';

import 'RandomWord.dart';
import 'Word.dart';
import 'package:chalkdart/chalk.dart';

class Table implements RandomWord {
  List<String> listWords = [];
  List<String>? allListWord;
  List<String>? historyWord;
  int remainingWord = 0;
  List<List<String>>? table;
  List<String> result = [];
  bool giveUp = false;
  List<List<String>>? tempTable;
  List<int> positionListWord = [];
  Table() {
    table = [
      ["+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+"],
      ["+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+"],
      ["+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+"],
      ["+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+"],
      ["+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+"],
      ["+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+"],
      ["+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+"],
      ["+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+"],
      ["+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+"],
      ["+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+"],
      ["+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+"],
      ["+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+"],
      ["+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+", "+"],
    ];
  }
  @override
  void randomWord() {
    int listSize = allListWord!.length;
    int count = 1; //error safety
    Random random = Random();
    while (true) {
      int randomNumber = random.nextInt(listSize);
      if (count == 1 || !listWords.contains(allListWord![randomNumber])) {
        listWords.add((allListWord![randomNumber].toLowerCase()));
        result.add(allListWord![randomNumber].toLowerCase());
        remainingWord++;
        count++;
      }
      if (listWords.length == 5) {
        break;
      }
    }
  }

  void showTable() {
    clearScreen();
    print(chalk.red.bold(
        "        a       b       c       d       e       f       g       h       i       j       k       l       m"));
    print(
        "        _       _       _       _       _       _       _       _       _       _       _       _       _\n");
    for (var i = 0, a = 65; i < table!.length; i++, a += 1) {
      stdout.write(chalk.red.bold(" ${String.fromCharCode(a)}"));
      stdout.write("   |   ");
      for (var j = 0; j < table![i].length; j++) {
        stdout.write("${table![i][j]}   |   ");
      }
      print("\n");
    }
  }

  void prepareTable() {
    listWords = [];
    positionListWord = [];
    result = [];
    remainingWord = 0;
    var word = Word();
    word.createFile("D:/BUU/Dart/projectgame2/bin/data/wordlist.txt");
    word.parse();
    allListWord = word.getListwords();
  }

  void randomWordIntoTable() {
    int range = table!.length;
    Random random = Random();
    // randomDirection 0:top 1:obliqueTopRight 2:right 3:obliqueUnderRight
    //4:under 5:obliqueUnderLeft 6:left 7:obliqueTopLeft

    // for (var i = 0; i < 5; i++) {

    while (listWords.length != 0) {
      int randomDirection = random.nextInt(8);
      //top check
      if (randomDirection == 0) {
        while (true) {
          int x = random.nextInt(range - listWords.first.length);
          int y = random.nextInt(range);
          x += listWords.first.length;
          if (checkTop(x, y) == listWords.first.length) {
            int count = 0;
            for (var i = 0; i < listWords.first.length; i++) {
              table![x][y] = listWords.first[count];
              positionListWord.add(x);
              positionListWord.add(y);
              count++;
              x--;
            }
            listWords.removeAt(0);
            break;
          } else {
            randomDirection = random.nextInt(8);
            break;
          }
        }
      } //obliqueTopRight check
      else if (randomDirection == 1) {
        while (true) {
          int x = random.nextInt(range - listWords.first.length);
          int y = random.nextInt(range - listWords.first.length);
          x += listWords.first.length;
          if (checkObliqueTopRight(x, y) == listWords.first.length) {
            int count = 0;
            for (var i = 0; i < listWords.first.length; i++) {
              table![x][y] = listWords.first[count];
              positionListWord.add(x);
              positionListWord.add(y);
              count++;
              x--;
              y++;
            }
            listWords.removeAt(0);
            break;
          } else {
            randomDirection = random.nextInt(8);
            break;
          }
        }
      } //right check
      else if (randomDirection == 2) {
        while (true) {
          int x = random.nextInt(range);
          int y = random.nextInt(range - listWords.first.length);
          if (checkRight(x, y) == listWords.first.length) {
            int count = 0;
            for (var i = 0; i < listWords.first.length; i++) {
              table![x][y] = listWords.first[count];
              positionListWord.add(x);
              positionListWord.add(y);
              count++;
              y++;
            }
            listWords.removeAt(0);
            break;
          } else {
            randomDirection = random.nextInt(8);
            break;
          }
        }
      } //obliqueUnderRight check
      else if (randomDirection == 3) {
        while (true) {
          int x = random.nextInt(range - listWords.first.length);
          int y = random.nextInt(range - listWords.first.length);
          if (checkObliqueUnderRight(x, y) == listWords.first.length) {
            int count = 0;
            for (var i = 0; i < listWords.first.length; i++) {
              table![x][y] = listWords.first[count];
              positionListWord.add(x);
              positionListWord.add(y);
              count++;
              y++;
              x++;
            }
            listWords.removeAt(0);
            break;
          } else {
            randomDirection = random.nextInt(8);
            break;
          }
        }
      } // under check
      else if (randomDirection == 4) {
        while (true) {
          int x = random.nextInt(range - listWords.first.length);
          int y = random.nextInt(range);
          if (checkUnder(x, y) == listWords.first.length) {
            int count = 0;
            for (var i = 0; i < listWords.first.length; i++) {
              table![x][y] = listWords.first[count];
              positionListWord.add(x);
              positionListWord.add(y);
              count++;
              x++;
            }
            listWords.removeAt(0);
            break;
          } else {
            randomDirection = random.nextInt(8);
            break;
          }
        }
      } // obliqueUnderLeft check
      else if (randomDirection == 5) {
        while (true) {
          int x = random.nextInt(range - listWords.first.length);
          int y = random.nextInt(range - listWords.first.length);
          y += listWords.first.length;
          if (checkObliqueUnderLeft(x, y) == listWords.first.length) {
            int count = 0;
            for (var i = 0; i < listWords.first.length; i++) {
              table![x][y] = listWords.first[count];
              positionListWord.add(x);
              positionListWord.add(y);
              count++;
              x++;
              y--;
            }
            listWords.removeAt(0);
            break;
          } else {
            randomDirection = random.nextInt(8);
            break;
          }
        }
      } // left check
      else if (randomDirection == 6) {
        while (true) {
          int x = random.nextInt(range - listWords.first.length);
          int y = random.nextInt(range - listWords.first.length);
          y += listWords.first.length;
          if (checkLeft(x, y) == listWords.first.length) {
            int count = 0;
            for (var i = 0; i < listWords.first.length; i++) {
              table![x][y] = listWords.first[count];
              positionListWord.add(x);
              positionListWord.add(y);
              count++;
              y--;
            }
            listWords.removeAt(0);
            break;
          } else {
            randomDirection = random.nextInt(8);
            break;
          }
        }
      } // obliqueTopLeft check
      else if (randomDirection == 7) {
        while (true) {
          int x = random.nextInt(range - listWords.first.length);
          int y = random.nextInt(range - listWords.first.length);
          y += listWords.first.length;
          x += listWords.first.length;
          if (checkLeft(x, y) == listWords.first.length) {
            int count = 0;
            for (var i = 0; i < listWords.first.length; i++) {
              table![x][y] = listWords.first[count];
              positionListWord.add(x);
              positionListWord.add(y);
              count++;
              y--;
              x--;
            }
            listWords.removeAt(0);
            break;
          } else {
            randomDirection = random.nextInt(8);
            break;
          }
        }
      }
      //}
    }
  }

  int checkTop(int x, int y) {
    int count = 0;
    for (var i = 0; i < listWords.first.length; i++) {
      if (table![x][y] == "+" && checkAlphabet(table![x][y]) == false) {
        x--;
        count++;
      }
    }
    return count;
  }

  int checkObliqueTopRight(int x, int y) {
    int count = 0;
    for (var i = 0; i < listWords.first.length; i++) {
      if (table![x][y] == "+" && checkAlphabet(table![x][y]) == false) {
        x--;
        y++;
        count++;
      }
    }
    return count;
  }

  int checkRight(int x, int y) {
    int count = 0;
    for (var i = 0; i < listWords.first.length; i++) {
      if (table![x][y] == "+" && checkAlphabet(table![x][y]) == false) {
        y++;
        count++;
      }
    }
    return count;
  }

  int checkObliqueUnderRight(int x, int y) {
    int count = 0;
    for (var i = 0; i < listWords.first.length; i++) {
      if (table![x][y] == "+" && checkAlphabet(table![x][y]) == false) {
        x++;
        y++;
        count++;
      }
    }
    return count;
  }

  int checkUnder(int x, int y) {
    int count = 0;
    for (var i = 0; i < listWords.first.length; i++) {
      if (table![x][y] == "+" && checkAlphabet(table![x][y]) == false) {
        x++;
        count++;
      }
    }
    return count;
  }

  int checkObliqueUnderLeft(int x, int y) {
    int count = 0;
    for (var i = 0; i < listWords.first.length; i++) {
      if (table![x][y] == "+" && checkAlphabet(table![x][y]) == false) {
        x++;
        y--;
        count++;
      }
    }
    return count;
  }

  int checkLeft(int x, int y) {
    int count = 0;
    for (var i = 0; i < listWords.first.length; i++) {
      if (table![x][y] == "+" && checkAlphabet(table![x][y]) == false) {
        y--;
        count++;
      }
    }
    return count;
  }

  int checkObliqueTopLeft(int x, int y) {
    int count = 0;
    for (var i = 0; i < listWords.first.length; i++) {
      if (table![x][y] == "+" && checkAlphabet(table![x][y]) == false) {
        y--;
        x--;
        count++;
      }
    }
    return count;
  }

  void fillNull() {
    String string = "abcdefghijklmnopkrstuvwxyz";
    Random random = Random();
    int index = random.nextInt(string.length);
    for (var i = 0; i < table!.length; i++) {
      for (var j = 0; j < table![i].length; j++) {
        if (table![i][j] == "+") {
          table![i][j] = string[index];
          index = random.nextInt(string.length);
        }
      }
    }
  }

  void showResult() {
    print(result);
  }

  void showReaminingWord() {
    print("Reamining Word: $remainingWord");
  }

  void checkResult(String input) {
    if (input == "giveup") {
      showHintAllWord();
      showTable();
      showReaminingWord();
      showResult();
      giveUp = true;

      return;
    }
    Map row = {
      "A": 0,
      "B": 1,
      "C": 2,
      "D": 3,
      "E": 4,
      "F": 5,
      "G": 6,
      "H": 7,
      "I": 8,
      "J": 9,
      "K": 10,
      "L": 11,
      "M": 12
    };
    Map col = {
      "a": 0,
      "b": 1,
      "c": 2,
      "d": 3,
      "e": 4,
      "f": 5,
      "g": 6,
      "h": 7,
      "i": 8,
      "j": 9,
      "k": 10,
      "l": 11,
      "m": 12
    };

    String tempInput = "";
    try {
      List<int> tempIndex = [];
      bool check = false;
      while (input.isNotEmpty) {
        tempIndex.add(row[input[0]]);
        tempIndex.add(col[input[1]]);
        tempInput += table![row[input[0]]][col[input[1]]];
        table![row[input[0]]][col[input[1]]] =
            chalk.green(table![row[input[0]]][col[input[1]]]);
        input = input.substring(2);
      }
      check = removeCheck(tempInput, check);
      removeFailedCheck(check, tempInput, tempIndex);
      endProcess();
    } catch (e) {}
  }

  void endProcess() {
    if (result.isEmpty) {
      clearScreen();
      showTable();
    }
  }

  void removeFailedCheck(bool check, String string, List<int> tempIndex) {
    if (check == false) {
      while (string.isNotEmpty) {
        print("in");
        table![tempIndex[0]][tempIndex[1]] = string[0];
        tempIndex.removeAt(0);
        tempIndex.removeAt(0);
        string = string.substring(1);
      }
    }
  }

  bool removeCheck(String string, bool check) {
    for (var i = 0; i < result.length; i++) {
      if (result[i] == string) {
        check = true;
        result.remove(string);
        remainingWord--;
        break;
      }
    }
    return check;
  }

  bool getGiveUp() {
    return giveUp;
  }

  void clearScreen() {
    print("\x1B[2J\x1B[0;0H");
    print(chalk.red("""
███████╗██╗███╗   ██╗██████╗     ██╗    ██╗ ██████╗ ██████╗ ██████╗      ██████╗  █████╗ ███╗   ███╗███████╗
██╔════╝██║████╗  ██║██╔══██╗    ██║    ██║██╔═══██╗██╔══██╗██╔══██╗    ██╔════╝ ██╔══██╗████╗ ████║██╔════╝
█████╗  ██║██╔██╗ ██║██║  ██║    ██║ █╗ ██║██║   ██║██████╔╝██║  ██║    ██║  ███╗███████║██╔████╔██║█████╗  
██╔══╝  ██║██║╚██╗██║██║  ██║    ██║███╗██║██║   ██║██╔══██╗██║  ██║    ██║   ██║██╔══██║██║╚██╔╝██║██╔══╝  
██║     ██║██║ ╚████║██████╔╝    ╚███╔███╔╝╚██████╔╝██║  ██║██████╔╝    ╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗
╚═╝     ╚═╝╚═╝  ╚═══╝╚═════╝      ╚══╝╚══╝  ╚═════╝ ╚═╝  ╚═╝╚═════╝      ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝
                                                                                                            
"""));
    print("Rule Of FindWord Game:");
    print("- This is a text search game. You'll need a long time to find it.");
    print(
        "- Enter the inputs in sequential order, starting with the row and then the column.");
    print(
        "- If you think you will give up You just type the word ${chalk.red.bold.blink("giveup")} and the game is over.");
    print(
        "you can win only when You have to find all the words in the list. But if you give up, you are the loser.");
    print(
        "- If you already understand the FindWord rules, please type Enter to start the game.\n");
  }

  void fillColor() {
    for (var i = 0, a = 65; i < table!.length; i++, a += 1) {
      for (var j = 0; j < table![i].length; j++) {
        if (checkAlphabet(table![i][j]) == true) {
          table![i][j] = chalk.red(table![i][j]);
        }
      }
    }
  }

  bool checkAlphabet(String string) {
    if ("abcdefghijklmnopqrstuvwxyz".contains(string)) {
      return true;
    }
    return false;
  }

  void showHintAllWord() {
    while (positionListWord.isNotEmpty) {
      int x = positionListWord.first;
      positionListWord.removeAt(0);
      int y = positionListWord.first;
      positionListWord.removeAt(0);
      table![x][y] = chalk.bold.red.blink(table![x][y]);
    }
    clearScreen();
    showTable();
  }
}
